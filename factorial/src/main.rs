extern crate num_bigint;
extern crate num_traits;

use num_bigint::BigUint;
use num_traits::{One};

fn factorial(n: usize) -> BigUint {
    let mut i: BigUint = One::one();

    for x in 1..(n + 1) {
        i *= x;
    }

    return i;
}

fn main() {
    println!("hello from rust");

    for i in 1..20 {
        println!("factorial of {} is {}", i, factorial(i));
    }

    println!("factorial of {} is {}", 5000, factorial(5000));
}
